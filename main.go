package main

import (
	"gitlab.com/gomoduledemo/demomodule/interfaces"
	"gitlab.com/gomoduledemo/demomodule/module1"
	"gitlab.com/gomoduledemo/demomodule/module2"
	"gitlab.com/gomoduledemo/demomodule/module3"
)

func main() {
	m1 := module1.NewModule("module1")
	m2 := module2.NewModule("module2")
	m3 := module3.NewModule("module3")
	print(m1)
	print(m2)
	print(m3)
}

func print(i interfaces.TestInterface) {
	i.PrintId()
}
