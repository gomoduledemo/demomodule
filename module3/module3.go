package module3

import "fmt"

type Module3 struct {
	id string
}

func NewModule(id string) *Module3 {
	return &Module3{id: id}
}

func (f *Module3) PrintId() {
	fmt.Printf("I am module 1 : Identifier %s \n", f.id)
}
