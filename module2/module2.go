package module2

import "fmt"

type Module2 struct {
	id string
}

func NewModule(id string) *Module2 {
	return &Module2{id: id}
}

func (f *Module2) PrintId() {
	fmt.Printf("I am module 1 : Identifier %s \n", f.id)
}
