package module1

import "fmt"

type Module1 struct {
	id string
}

func NewModule(id string) *Module1 {
	return &Module1{id: id}
}

func (f *Module1) PrintId() {
	fmt.Printf("I am module 1 : Identifier %s \n", f.id)
}
